/*
  File: Handlebars.ino
  Date: 14/05/2014
  Purpose: Changes state between off, blink left, and blink right depending upon 
           the state of two buttons
  Author: Kierran McPherson
*/

//Declare a global variable to hold the state
String state;

//Declare the constants
const String OFF = "OFF";
const String INDICATE_LEFT = "LEFT";
const String INDICATE_RIGHT = "RIGHT";
const int INDICATOR_DELAY = 300;

//Variables to store the times buttons were last pressed
unsigned long bLeftLastPush = 0;
unsigned long bRightLastPush = 0;

//The pins of the left and right indicators
int leftLight = 4;
int rightLight = 5;

//The pins of the left and right buttons
int leftButton = 2;
int rightButton = 3;

//A flag to hold whether or not the helmet has been updated
boolean helmetUpdated = false;

void setup() {
    //Begin listening to the Serial port
    Serial.begin(9600);
    state = OFF;
    
    //Set the button pins to be inputs
    pinMode(leftButton, INPUT);
    pinMode(rightButton, INPUT);
    
    //Set the indicator pins to be outputs
    pinMode(leftLight, OUTPUT);
    pinMode(rightLight, OUTPUT);
    
    //Attach the interrupt for the left button
    attachInterrupt(0, leftButtonPressed, RISING);
    //Attach the interrupt for the right button
    attachInterrupt(1, rightButtonPressed, RISING);
}

void loop() {
   //If the state has changed since last loop
   if(!helmetUpdated)
   {
     //If the state hsa been set to OFF
     if(state == OFF)
     {
       //Update the helmet
       Serial.print(0);
     }
     else
     {
        //If the state has been changed to INDICATE_LEFT
        if(state == INDICATE_LEFT)
        {
          //Update the helmet
          Serial.print(1);
        }
        else
        {
           //If the state has been changed to INDICATE_RIGHT
           if(state == INDICATE_RIGHT)
           {
             //Update the helmet
             Serial.print(2);
           } 
        }
     }
     
     //Mark the hemlet as updated
     helmetUpdated = true;
   }
   
   //If the current state is not off
   if(state != OFF)
   {
     //Check if the state is set to left indicate
      if(state == INDICATE_LEFT)
      {
        //Blink the left indicator
        blinkPin(leftLight);
      }
      else
      {
        //Check if the state is set to right indicate
         if(state == INDICATE_RIGHT)
         {
            //Blink the right indicator
            blinkPin(rightLight);
         } 
      }
   }
}

//Blinks a pin on and off
void blinkPin(int pin)
{
  digitalWrite(pin, HIGH);
  delay(INDICATOR_DELAY);
  digitalWrite(pin, LOW);
  delay(INDICATOR_DELAY);
}

//The method called when the leftButton interrupt is triggered
void leftButtonPressed()
{
    //If 100ms have passed since this button was last pushed
    if((millis() - bLeftLastPush) > 200)
    {
        //if the state was previously set to OFF or INDICATE_RIGHT
        if(state == OFF || state == INDICATE_RIGHT)
        {
            state = INDICATE_LEFT;
        }
        else //The light was indicating so turn it off
        {
           state = OFF; 
        }
        
        //Mark the helmet as needing updating
        helmetUpdated = false;
        //Record the time this button was pushed
        bLeftLastPush = millis();
    }
}

//The method called when the rightButton interrupt is triggered
void rightButtonPressed()
{
    //If 100ms have passed since this button was last pushed
    if((millis() - bRightLastPush) > 200)
    {
        //if the state was previously set to OFF or INDICATE_LEFT
        if(state == OFF || state == INDICATE_LEFT)
        {
            state = INDICATE_RIGHT;
        }
        else //The light was indicating so turn it off
        {
           state = OFF; 
        }
        
        //Mark the helmet as needing updating
        helmetUpdated = false;
        //Record the time this button was pushed
        bRightLastPush = millis();
    }
}
