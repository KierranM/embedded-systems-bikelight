/*
  File: Helmet.ino
  Date: 14/05/2014
  Purpose: Changes state between off, blink left, and blink right depending upon 
           what is recieved from the Serial
  Author: Kierran McPherson
*/

//Declare a global variable to hold the state
String state;

//Declare the constants
const String OFF = "OFF";
const String INDICATE_LEFT = "LEFT";
const String INDICATE_RIGHT = "RIGHT";
const int INDICATOR_DELAY = 300;

//The pins of the left and right indicators
int leftLight = 2;
int rightLight = 3;


void setup() {
  //Begin listening to the Serial port
  Serial.begin(9600);
  state = OFF;
  
  //Set the indicator pins to be outputs
  pinMode(2, OUTPUT);
  pinMode(3, OUTPUT);
}

void loop() {
  
  //Check if there are any available bytes in the buffer
 if(Serial.available())
 {
   //Read the byte from the buffer as an int
   int value = Serial.parseInt();
   
   //Switch on the value that was read in and set the state to match it
   switch (value)
   {
      case 0:
         state = OFF;
         break;
      case 1:
         state = INDICATE_LEFT;
         break;
      case 2:
         state = INDICATE_RIGHT;
         break;
   }
 }
 
 //If the current state is not off
 if(state != OFF)
 {
   //Check if the state is set to left indicate
    if(state == INDICATE_LEFT)
    {
      //Blink the left indicator
      blinkPin(leftLight);
    }
    else
    {
      //Check if the state is set to right indicate
       if(state == INDICATE_RIGHT)
       {
          //Blink the right indicator
          blinkPin(rightLight);
       } 
    }
 }
}

//Blinks a pin on and off
void blinkPin(int pin)
{
  digitalWrite(pin, HIGH);
  delay(INDICATOR_DELAY);
  digitalWrite(pin, LOW);
  delay(INDICATOR_DELAY);
}
